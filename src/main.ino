
#include <Arduboy2.h>
#include "HID-Project.h"

Arduboy2 arduboy;

const unsigned int FRAME_RATE = 15; // Frame rate in frames per second
char command[20] = {0};
long previousMillis = 0;
long interval = 500;

void setup() {
  arduboy.begin();
  arduboy.setFrameRate(FRAME_RATE);
  arduboy.setTextWrap(true);
  arduboy.setTextSize(3);

  Keyboard.begin();
}

void loop() {
  // pause render until it's time for the next frame
  if (!(arduboy.nextFrame())) {
    return;
  }
  arduboy.pollButtons();
  unsigned long currentMillis = millis();

  if(currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    arduboy.clear();
  }

  arduboy.setCursor(0, 0);

  if (arduboy.justPressed(UP_BUTTON)) {
    Keyboard.write(MEDIA_VOLUME_UP);
    arduboy.print(F("Volume Up"));
  }

  if (arduboy.justPressed(RIGHT_BUTTON)) {
    Keyboard.write(CONSUMER_BRIGHTNESS_UP);
    arduboy.print(F("Brightness Up"));
  }

  if (arduboy.justPressed(DOWN_BUTTON)) {
    Keyboard.write(MEDIA_VOLUME_DOWN);
    arduboy.print(F("Volume Down"));
  }

  if (arduboy.justPressed(LEFT_BUTTON)) {
    Keyboard.write(CONSUMER_BRIGHTNESS_DOWN);
    arduboy.print(F("Brightness Down"));
  }

  if(arduboy.justPressed(A_BUTTON)) {
    Keyboard.write(MEDIA_PLAY_PAUSE);
    arduboy.print(F("Play/Pause"));
  }

  if (arduboy.justPressed(B_BUTTON)) {
    Keyboard.write(MEDIA_VOLUME_MUTE);
    arduboy.print(F("Mute"));
  }

  arduboy.display();
}

